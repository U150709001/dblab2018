
#what are the number of customers located in each country
select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by customers.Country
order by numberOfCustomers desc
;

#the number of products that we buy from each suppliers
select count(products.ProductID) as numberOfProducts, suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by numberOfProducts desc
;




load data local infile "C:\\Kullanıcılar\\Eda\\Masaüstü\\customers.csv "
into table customers 
fields terminated by ';'
ignore 1 lines;